#include "stdafx2.h"

int bmtp_log_init( struct bmtp_context *bmtp );
int bmtp_log_add( struct bmtp_context *bmtp, const char *fmt, ... );
int bmtp_log_print( struct bmtp_context *bmtp, const char *fmt, ... );
