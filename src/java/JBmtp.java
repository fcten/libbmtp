package bmtp.bmtp;

/**
 * Created by fcten_000 on 2016/8/12.
 */
public class JBmtp {
    static {
        System.loadLibrary("bmtp");
    }

    public native boolean open( String ip, int port );
    public native boolean pub( String data, int qos );
    public native boolean sub( long channel_id );

    public void onOpen() {
        sub(1);
    }

    public void onClose() {

    }
    public void onError( int errNo ) {

    }
    public void onMessage( byte[] data ) {
        String msg = new String(data);

    }
}

